module auth.token.rsa {
	requires auth.token.core;
	exports auth.token.rsa;
	
	requires auth0.jwt;
	requires vmj.auth;
	requires vmj.routing.route;
}