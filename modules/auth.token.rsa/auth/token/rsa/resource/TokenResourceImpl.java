package auth.token.rsa;

import java.util.HashMap;

import auth.token.core.TokenResourceComponent;
import auth.token.core.TokenResourceDecorator;
import auth.token.rsa.utils.KeyGeneratorUtils;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.routing.route.exceptions.*;

public class TokenResourceImpl extends TokenResourceDecorator {

    public TokenResourceImpl(TokenResourceComponent record) {
        super(record);
    }

    @Route(url="call/token/publickey")
    public HashMap<String,Object> getTokenPublicKey(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        String public_key = KeyGeneratorUtils.getPublicKey();
        if (public_key.isEmpty()) {
            throw new FileNotFoundException();
        }
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("public_key", public_key);
        return result;
    }
}