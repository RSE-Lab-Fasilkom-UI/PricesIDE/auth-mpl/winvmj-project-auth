package auth.token.rsa;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;

import com.auth0.jwt.algorithms.Algorithm;
import auth.token.core.TokenComponent;
import auth.token.core.TokenDecorator;
import auth.token.rsa.utils.KeyGeneratorUtils;

public class TokenImpl extends TokenDecorator {

	private final String name = "RSA";
	private final int keyLength = 2048;

	public TokenImpl(TokenComponent token) {
		super(token);
	}
	
	public TokenImpl() {
		super();
	}
	
	@Override
	public Algorithm getSigningAlgorithm() {
		RSAPrivateKey privateKey = null;
		try {
			privateKey = getPrivateKey();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.out.println("Token problem: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
    	return Algorithm.RSA256(null, privateKey);
    }
	
	@Override
	public Algorithm getValidationAlgorithm() {
		RSAPublicKey publicKey = null;
		try {
			publicKey = getPublicKey();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.out.println("Token problem: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
        return Algorithm.RSA256(publicKey, null);
    }
	
	private RSAPrivateKey getPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
		PrivateKey privateKey = KeyGeneratorUtils.getPrivateKey(name, keyLength);
		return (RSAPrivateKey) privateKey;
	}
	
	private RSAPublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
		PublicKey publicKey = KeyGeneratorUtils.getPublicKey(name, keyLength);
		return (RSAPublicKey) publicKey;
    }
}
