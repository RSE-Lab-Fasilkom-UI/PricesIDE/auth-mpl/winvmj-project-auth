module auth.verifier.basic {
	requires auth.token.core;
	requires auth.verifier.core;
	exports auth.verifier.basic;
	
	requires auth0.jwt;
	requires vmj.auth;
}