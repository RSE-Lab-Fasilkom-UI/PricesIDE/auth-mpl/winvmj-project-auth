module auth.management.directory {
	requires auth.management.core;
	exports auth.management.directory;
	
	requires auth.verifier.ldap;
	requires vmj.hibernate.integrator;
	requires vmj.routing.route;
	requires vmj.auth;
}