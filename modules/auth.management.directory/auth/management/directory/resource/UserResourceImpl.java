package auth.management.directory;

import java.util.*;

import auth.management.UserFactory;
import auth.management.UserRoleFactory;
import auth.management.core.Role;
import auth.management.core.User;
import auth.management.core.UserComponent;
import auth.management.core.UserDecorator;
import auth.management.core.UserResourceComponent;
import auth.management.core.UserResourceDecorator;
import auth.management.core.UserRoleImpl;
import auth.management.core.RoleComponent;
import auth.management.utils.KeepLoginUtils;
import auth.management.utils.PasswordUtils;
import auth.management.utils.PermissionUtils;
import auth.verifier.ldap.utils.LDAPUtils;

import vmj.auth.annotations.Restricted;
import vmj.auth.exceptions.AuthException;
import vmj.hibernate.integrator.RepositoryUtil;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.routing.route.exceptions.*;

public class UserResourceImpl extends UserResourceDecorator {
    public RepositoryUtil<UserRoleImpl> userRoleDao;
    public RepositoryUtil<Role> roleDao;
    
    public UserResourceImpl(UserResourceComponent record) {
        super(record);
        this.userRoleDao = new RepositoryUtil<UserRoleImpl>(auth.management.core.UserRoleImpl.class);
        this.roleDao = new RepositoryUtil<Role>(auth.management.core.RoleComponent.class);
    }
    
    private String getVerifier() {
    	return LDAPUtils.getName();
    }

    @Route(url = "auth/login/ldap")
    public HashMap<String, Object> saveUser(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        Object username = vmjExchange.getRequestBodyForm("username");
        Object password = vmjExchange.getRequestBodyForm("password");
        HashMap<String, String> userInformation = LDAPUtils.getUserInformation((String)username, (String)password);
        if (userInformation.size() < 3) {
            throw new AuthException("Informasi pengguna yang diperlukan tidak tersedia");
        }

        String verifier = getVerifier();
        String name = userInformation.get("name");
        String email = userInformation.get("email");
        String socialId = userInformation.get("social_id");
        List<User> userLdapList = userDao.getListObject("auth_user_directory", "socialid", socialId);
        HashMap<String, Object> value = new HashMap<>();
        if (userLdapList.size() > 0){
            UserImpl userLdap = (UserImpl) userLdapList.get(0);
            UUID userLdapId = userLdap.getUser().getId();
            ArrayList<String> allowedPerms = PermissionUtils.getUserAllowedPerms(userLdapId, userLdap.getAllowedPermissions(), verifier);
            value = userLdap.toHashMap();
            value.put("allowedPermissions", allowedPerms);
            value.put("token", PasswordUtils.generateAuthToken(userLdap.getId().toString(), userLdap.getEmail(), verifier));
            value.put("token_keep_login", KeepLoginUtils.generateKeepLoginToken(userLdap.getId().toString(), userLdap.getEmail(), String.join(",", allowedPerms), verifier));
            return value;
        }
        else {
            List<User> users = userDao.getListObject("auth_user_impl", "email", email);
            if (!users.isEmpty()) {
                User user = users.get(0);
                ArrayList<String> allowedPerms = PermissionUtils.getUserAllowedPerms(user.getId(), user.getAllowedPermissions(), verifier);
                User userLdap = UserFactory.createUser("auth.management.directory.UserImpl", user, socialId);   
                userDao.saveObject(userLdap);
                value = userLdap.toHashMap();
                value.put("allowedPermissions", allowedPerms);
                value.put("token", PasswordUtils.generateAuthToken(userLdap.getId().toString(), userLdap.getEmail(), verifier));
                value.put("token_keep_login", KeepLoginUtils.generateKeepLoginToken(userLdap.getId().toString(), userLdap.getEmail(), String.join(",", allowedPerms), verifier));
                return value;
            }
            UserImpl userLdap = (UserImpl) createUser(name, email, socialId);
            userDao.saveObject(userLdap);
            User user = userLdap.getUser();
            List<Role> roles = roleDao.getListObject("auth_role_impl", "name", "Registered");
            if (roles.size() > 0) {
                Role role = roles.get(0);
                UserRoleImpl userRole = UserRoleFactory.createUserRole("auth.management.core.UserRoleImpl", role, user);
                userRoleDao.saveObject(userRole);
            }
            ArrayList<String> allowedPerms = PermissionUtils.getUserAllowedPerms(user.getId(), userLdap.getAllowedPermissions(), verifier);
            value = userLdap.toHashMap();
            value.put("allowedPermissions", allowedPerms);
            value.put("token", PasswordUtils.generateAuthToken(userLdap.getId().toString(), userLdap.getEmail(), verifier));
            value.put("token_keep_login", KeepLoginUtils.generateKeepLoginToken(userLdap.getId().toString(), userLdap.getEmail(), String.join(",", allowedPerms), verifier));
            return value;
        }
    }
    
    public User createUser(String name, String email, String socialId) {
        User user = UserFactory.createUser("auth.management.core.UserImpl", name, email);
        userDao.saveObject(user);
        User userLdap = UserFactory.createUser("auth.management.directory.UserImpl", user, socialId);
        return userLdap;
    }

    @Restricted(permissionName = "administrator")
    @Route(url="call/user-directory/delete")
    public List<HashMap<String,Object>> deleteUser(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        UUID idUser = UUID.fromString(idStr);
        User user = userDao.getObject(idUser);
        if (user.getEmail().equals(vmjExchange.getAuthPayload().getEmail())){
            return record.getAllUser(vmjExchange);
        }
        else {
            List<User> userDirectoryList = userDao.getListObject("auth_user_directory", "user_id", idUser);
    
            List<UserRoleImpl> userRoles = userRoleDao.getListObject("auth_user_role_impl", "authuser", idUser);
            for (UserRoleImpl userRole : userRoles) {
                userRoleDao.deleteObject(userRole.getId());
            }
    
            if (userDirectoryList.size() > 0){
                UserDecorator userDirectory = (UserDecorator) userDirectoryList.get(0);
                UUID idUserDirectory = userDirectory.getId();
                userDao.deleteObject(idUserDirectory);
            }
            return record.deleteUser(vmjExchange);
        }
    }
}
