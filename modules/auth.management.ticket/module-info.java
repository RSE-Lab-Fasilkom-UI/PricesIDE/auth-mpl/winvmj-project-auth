module auth.management.ticket {
	requires auth.management.core;
	exports auth.management.ticket;
	
	requires auth.verifier.cas;
	requires java.persistence;
	requires vmj.hibernate.integrator;
	requires vmj.routing.route;
	requires vmj.auth;
}