package auth.management.defaultverifier;

import java.util.HashMap;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import auth.management.core.RoleComponent;
import auth.management.core.RoleDecorator;

@Entity(name="auth_role_verifier")
@Table(name="auth_role_verifier")
public class RoleImpl extends RoleDecorator {
    @Column
    public String verifier;

    public RoleImpl() {
        super();
    }

    public RoleImpl(UUID id, RoleComponent role, String verifier) {
        super(id, role);
        this.verifier = verifier;
    }
    
    public RoleImpl(RoleComponent role, String verifier) {
        super(role);
        this.verifier = verifier;
    }

    public void setVerifier(String verifier) {
        this.verifier = verifier;
    }

    public String getVerifier() {
        return this.verifier;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> roleMap = role.toHashMap();
        roleMap.put("verifier", verifier);
        return roleMap;
    }
}
