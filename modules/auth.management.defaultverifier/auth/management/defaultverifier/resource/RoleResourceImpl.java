package auth.management.defaultverifier;

import java.util.*;

import auth.management.RoleFactory;
import auth.management.core.Role;
import auth.management.core.RoleResourceComponent;
import auth.management.core.RoleResourceDecorator;
import auth.management.core.UserRoleImpl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import vmj.auth.annotations.Restricted;
import vmj.hibernate.integrator.HibernateUtil;
import vmj.hibernate.integrator.RepositoryUtil;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

public class RoleResourceImpl extends RoleResourceDecorator {
    public RepositoryUtil<UserRoleImpl> userRoleDao;

    public RoleResourceImpl(RoleResourceComponent record) {
        super(record);
        this.userRoleDao = new RepositoryUtil<UserRoleImpl>(auth.management.core.UserRoleImpl.class);
    }

    public Map<String,List<Map<String,Object>>> transformDefaultRoleListToHashMap(List<Role> roleList) {
        Map<String,List<Map<String,Object>>> resultList = new HashMap<String, List<Map<String,Object>>>();
        for(int i = 0; i < roleList.size(); i++) {
            Map<String,Object> roleDataMap = ((RoleImpl)roleList.get(i)).toHashMap();
            String verifier = (String)roleDataMap.get("verifier");
            if (resultList.containsKey(verifier)) {
                List<Map<String,Object>> listRole = resultList.get(verifier);
                listRole.add(roleDataMap);
            } else {
                List<Map<String,Object>> newListRole = new ArrayList<Map<String,Object>>();
                newListRole.add(roleDataMap);
                resultList.put(verifier, newListRole);
            }
        }
        return resultList;
    }

    public Role createRole(VMJExchange vmjExchange) {
        String verifier = (String) vmjExchange.getRequestBodyForm("verifier");
        String name = (String) vmjExchange.getRequestBodyForm("name");
        String allowedPermissions = (String) vmjExchange.getRequestBodyForm("allowedPermissions");
        Role role = RoleFactory.createRole("auth.management.core.RoleImpl", name, allowedPermissions);
        roleDao.saveObject(role);
        return RoleFactory.createRole("auth.management.defaultverifier.RoleImpl", role, verifier);
    }

    @Restricted(permissionName = "administrator")
    @Route(url="call/default-verifier/list")
    public Map<String,List<Map<String,Object>>> getAllDefaultRole(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        List<Role> roles = roleDao.getAllObject("auth_role_verifier");
        Map<String,List<Map<String,Object>>> roleDatas = transformDefaultRoleListToHashMap(roles);
        return roleDatas;
    }

    private ArrayList<UUID> splitRoleId(String roleIds) {
        List<String> tempIds = Arrays.asList(roleIds.split(","));
        ArrayList<UUID> uuidIds = new ArrayList<UUID>();
        try{
            for (String id : tempIds) {
                if (id != ""){
                    uuidIds.add(UUID.fromString(id.trim()));
                }
            }
        } catch (Exception e){
            return uuidIds;
        }
        return uuidIds;
    }

    @Restricted(permissionName = "administrator")
    @Route(url="call/default-verifier/update")
    public Map<String,List<Map<String,Object>>> updateDefaultRole(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        String verifier = (String) vmjExchange.getRequestBodyForm("verifier");
        verifier = verifier.toLowerCase();

        String roleIds = (String) vmjExchange.getRequestBodyForm("roleIds");
        if (roleIds == null){
            roleIds = "";
        }
        ArrayList<UUID> arrRoleIds = splitRoleId(roleIds);
        List<Role> roles = roleDao.getListObject("auth_role_verifier", "verifier", verifier);
        List<UUID> existedRolesId = new ArrayList<UUID>();
        
        for (Role role : roles) {
            UUID idRole = ((RoleImpl)role).getRole().getId();
            existedRolesId.add(idRole);
        }
        
        List<Role> deletedRole = new ArrayList<Role>();
        for (Role role : roles) {
            UUID idRole = ((RoleImpl)role).getRole().getId();
            if (!arrRoleIds.contains(idRole)) {
                deletedRole.add(role);
            }
        }
        deleteDefaultVerifier(deletedRole);

        for (UUID roleId : arrRoleIds) {
            if (!existedRolesId.contains(roleId)){
                Role role = roleDao.getObject(roleId);
                Role roleVerifier = RoleFactory.createRole("auth.management.defaultverifier.RoleImpl", role, verifier);
                roleDao.saveObject(roleVerifier);
            }
        }
        
        roles = roleDao.getListObject("auth_role_verifier", "verifier", verifier);
        Map<String,List<Map<String,Object>>> roleDatas = transformDefaultRoleListToHashMap(roles);
        return roleDatas;
    }

    private void deleteDefaultVerifier(List<Role> deletedRole) {
        Transaction transaction = null;
        try (Session session = (HibernateUtil.getSessionFactory()).openSession()) {
            transaction = session.beginTransaction();
            for (Role role : deletedRole) {
                session.createQuery("DELETE from auth.management.defaultverifier.RoleImpl WHERE id ='" + role.getId() + "'").executeUpdate();
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}
