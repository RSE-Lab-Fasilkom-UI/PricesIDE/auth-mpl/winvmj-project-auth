module auth.management.defaultverifier {
	requires auth.management.core;
	exports auth.management.defaultverifier;

	requires vmj.auth;
	requires vmj.routing.route;
	requires vmj.hibernate.integrator;
	requires java.persistence;
}