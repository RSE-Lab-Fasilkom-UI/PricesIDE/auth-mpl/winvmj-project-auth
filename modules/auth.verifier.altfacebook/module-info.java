module auth.verifier.altfacebook {
	requires auth.verifier.core;
	exports auth.verifier.altfacebook;

	requires vmj.auth;
	requires auth0.jwt;
}