/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.verifier.altfacebook;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.HashMap;
import java.security.GeneralSecurityException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import auth.verifier.core.VerifierComponent;
import auth.verifier.core.VerifierDecorator;
import auth.verifier.altfacebook.adapters.FacebookPayloadAdapter;

import vmj.auth.core.AuthPayload;
import vmj.auth.core.TokenPayload;
import vmj.auth.utils.ConnectionUtils;
import vmj.auth.utils.PropertiesReader;

/**
 *
 * @author Ichlasul Affan
 */
public class VerifierImpl extends VerifierDecorator {

    public VerifierImpl(VerifierComponent verifier) {
		super(verifier);
        this.name = "facebook";
	}

    /**
     * @param rawClientId
     * @param token
     * @return
     */
    @Override
    public AuthPayload verifyAndParse(final String token) {
        PropertiesReader propertiesReader = new PropertiesReader();
        String clientId = propertiesReader.getClientId(this.name);
        String clientSecret = propertiesReader.getClientSecret(this.name);

        Map<String, Object> payloads = new HashMap<>();
        try {
            String appToken = getAppToken(clientId, clientSecret);
            payloads.putAll(getTokenData(appToken.trim(), token.trim()));
            ((Map) payloads.get("data")).putAll(getUserData(token.trim()));
            TokenPayload payload = new FacebookPayloadAdapter(payloads);

            if (payload.getEmail() == null) {
                throw new GeneralSecurityException("Invalid ID token");
            }
            return payload;
        } catch (GeneralSecurityException e) {
            System.out.println("Security issue: " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (MalformedURLException e) {
            System.out.println("Network problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Network problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        }

        return verifier.verifyAndParse(token);
    }

    private String getAppToken(final String clientId, final String clientSecret)
            throws MalformedURLException, IOException {
        return (String) ConnectionUtils.getJsonLessSecure("https://graph.facebook.com/oauth/access_token?client_id="
                + clientId + "&client_secret=" + clientSecret + "&grant_type=client_credentials")
                .getOrDefault("access_token", "");
    }

    private Map<String, Object> getTokenData(final String appToken, final String userToken)
            throws MalformedURLException, IOException {
        return ConnectionUtils.getJsonLessSecure("https://graph.facebook.com/debug_token?input_token="
                + userToken.trim() + "&access_token=" + appToken.trim());
    }

    private Map<String, Object> getUserData(final String token) throws MalformedURLException, IOException {
        return ConnectionUtils.getJsonLessSecure("https://graph.facebook.com/me?access_token="
                + token.trim() + "&fields=id,name,email");
    }

    @Override
    public boolean isValid(final String token) {
		try {
            DecodedJWT decodedJWT = JWT.decode(token);
            String verifierName = decodedJWT.getClaim("verifier").asString();
            if (verifierName != null) {
                return verifierName.equals(this.name);
            }
            return false;
        } catch (JWTDecodeException e) {
            return true;
        }
    }
}
