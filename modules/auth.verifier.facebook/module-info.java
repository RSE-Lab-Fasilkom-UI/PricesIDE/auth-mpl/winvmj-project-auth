module auth.verifier.facebook {
	requires auth.verifier.core;
	exports auth.verifier.facebook;

	requires vmj.auth;
	requires auth0.jwt;
}