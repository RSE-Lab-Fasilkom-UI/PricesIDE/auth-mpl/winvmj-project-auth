module auth.token.core {
	exports auth.token;
	exports auth.token.core;
	
	requires auth0.jwt;
	requires java.logging;
	requires vmj.auth;
	requires vmj.routing.route;
}