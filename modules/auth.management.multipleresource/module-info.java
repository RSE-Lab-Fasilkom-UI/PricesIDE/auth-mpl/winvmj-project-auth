module auth.management.multipleresource {
	requires auth.management.core;
	exports auth.management.multipleresource;

	requires vmj.auth;
	requires vmj.routing.route;
	requires vmj.hibernate.integrator;
	requires java.persistence;
}