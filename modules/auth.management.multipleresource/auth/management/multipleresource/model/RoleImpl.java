package auth.management.multipleresource;

import java.util.HashMap;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import auth.management.core.RoleComponent;
import auth.management.core.RoleDecorator;

@Entity(name="auth_role_resource")
@Table(name="auth_role_resource")
public class RoleImpl extends RoleDecorator {
    @Column
    public String resource;

    public RoleImpl() {
        super();
    }

    public RoleImpl(UUID id, RoleComponent role, String resource) {
        super(id, role);
        this.resource = resource;
    }
    
    public RoleImpl(RoleComponent role, String resource) {
        super(role);
        this.resource = resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return this.resource;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> roleMap = role.toHashMap();
        roleMap.put("id", this.id);
        roleMap.put("role_id", role.id);
        roleMap.put("resource", resource);
        return roleMap;
    }
}
