package auth.management.multipleresource;

import java.util.*;

import vmj.auth.annotations.Restricted;
import auth.management.RoleFactory;
import auth.management.core.Role;
import auth.management.core.UserRoleImpl;
import auth.management.core.RoleResourceComponent;
import auth.management.core.RoleResourceDecorator;
import auth.management.utils.PermissionUtils;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.hibernate.integrator.RepositoryUtil;

public class RoleResourceImpl extends RoleResourceDecorator {
    public RepositoryUtil<UserRoleImpl> userRoleDao;

    public RoleResourceImpl(RoleResourceComponent record) {
        super(record);
        this.userRoleDao = new RepositoryUtil<UserRoleImpl>(auth.management.core.UserRoleImpl.class);
    }

    @Restricted(permissionName = "administrator")
    @Route(url = "call/role-resource/save")
    public List<HashMap<String,Object>> saveRole(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        Role role = createRole(vmjExchange);
        roleDao.saveObject(role);
        return getAllRole(vmjExchange);
    }

    public Role createRole(VMJExchange vmjExchange) {
        String resource = (String) vmjExchange.getRequestBodyForm("resource");
        resource = resource.toLowerCase();
        String name = (String) vmjExchange.getRequestBodyForm("name");
        name = name.startsWith("#") ? name : "#" + name;
        String allowedPermissions = (String) vmjExchange.getRequestBodyForm("allowedPermissions");
        Role role = RoleFactory.createRole("auth.management.core.RoleImpl", name, allowedPermissions);
        roleDao.saveObject(role);
        return RoleFactory.createRole("auth.management.multipleresource.RoleImpl", role, resource);
    }

    public List<HashMap<String,Object>> transformRoleListToHashMap(List<Role> roleList) {
        List<HashMap<String,Object>> resultList = new ArrayList<HashMap<String,Object>>();
        for(int i = 0; i < roleList.size(); i++) {
            HashMap<String,Object> roleDataMap = ((RoleImpl)roleList.get(i)).toHashMap();
            roleDataMap.put("allowedPermissions", PermissionUtils.splitPermissions((String)roleDataMap.get("allowedPermissions")));
            resultList.add(roleDataMap);
        }

        return resultList;
    }

    @Restricted(permissionName = "administrator")
    @Route(url="call/role-resource/list")
    public List<HashMap<String,Object>> getAllRole(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        List<Role> roles = roleDao.getAllObject("auth_role_resource");
        List<HashMap<String,Object>> roleDatas = transformRoleListToHashMap(roles);
        return roleDatas;
    }

    @Restricted(permissionName = "administrator")
    @Route(url="call/role-resource/delete")
    public List<HashMap<String,Object>> deleteRole(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        UUID id = UUID.fromString(idStr);
        Role role = roleDao.getObject(id);
        List<UserRoleImpl> userRoles = userRoleDao.getListObject("auth_user_role_impl", "authrole", role.getId());
        for (UserRoleImpl userRole : userRoles) {
            userRoleDao.deleteObject(userRole.getId());
        }
        roleDao.deleteObject(id);
        return getAllRole(vmjExchange);
    }

    public Role updateRole(VMJExchange vmjExchange, UUID id) {
        String name = (String) vmjExchange.getRequestBodyForm("name");
        name = name.startsWith("#") ? name : "#" + name;
        String allowedPermissions = (String) vmjExchange.getRequestBodyForm("allowedPermissions");
        String resource = (String) vmjExchange.getRequestBodyForm("resource");
        resource = resource.toLowerCase();
        Role role = roleDao.getObject(id);
        role.setName(name);
        role.setAllowedPermissions(allowedPermissions);
        ((RoleImpl)role).setResource(resource);
        return role;
    }

    @Restricted(permissionName = "administrator")
    @Route(url="call/role-resource/update")
    public HashMap<String, Object> updateRole(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        UUID id = UUID.fromString(idStr);

        Role role = roleDao.getObject(id);
        role = updateRole(vmjExchange, id);
        roleDao.updateObject(role);
        HashMap<String,Object> roleDataMap = role.toHashMap();
        roleDataMap.put("allowedPermissions", PermissionUtils.splitPermissions((String)roleDataMap.get("allowedPermissions")));
        return roleDataMap;
    }

    @Restricted(permissionName = "administrator")
    @Route(url="call/role-resource/detail")
    public HashMap<String, Object> getRole(VMJExchange vmjExchange) {
        if (vmjExchange.getHttpMethod().equals("OPTIONS")) {
            return null;
        }
        String idStr = vmjExchange.getGETParam("id");
        UUID id = UUID.fromString(idStr);
        Role role = roleDao.getObject(id);
        HashMap<String,Object> roleDataMap = role.toHashMap();
        roleDataMap.put("allowedPermissions", PermissionUtils.splitPermissions((String)roleDataMap.get("allowedPermissions")));
        roleDataMap.put("resource", ((RoleImpl)role).getResource());
        return roleDataMap;
    }
}
