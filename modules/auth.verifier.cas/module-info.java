module auth.verifier.cas {
	requires auth.verifier.core;
	exports auth.verifier.cas;
	exports auth.verifier.cas.utils;

	requires vmj.auth;
	requires cas.client;
}