package auth.verifier.cas.adapters;

import org.jasig.cas.client.validation.Assertion;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

import vmj.auth.core.TokenPayload;
import vmj.auth.utils.PropertiesReader;

public class CASPayloadAdapter implements TokenPayload {
    final private Assertion assertion;
	
    public CASPayloadAdapter(Assertion assertion) {
        this.assertion = assertion;
    }

    @Override
    public String getEmail() {
        PropertiesReader propertiesReader = new PropertiesReader();
        String emailKey = propertiesReader.getPropOrDefault("cas.email_key", "mail");
        Map attrs = assertion.getPrincipal().getAttributes();
        if (attrs.get(emailKey) != null) 
            return (String)attrs.get(emailKey);
        return "";
    }

    @Override
    public List<String> getAudiences() {
        List<String> result = new ArrayList<>();
        return result;
    }

    @Override
    public String getIssuer() {
        return "";
    }
}
