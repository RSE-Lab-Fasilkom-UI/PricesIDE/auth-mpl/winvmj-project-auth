package auth.verifier.cas.utils;

import java.util.*;

import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.jasig.cas.client.validation.TicketValidator;

import vmj.auth.utils.PropertiesReader;

public class CASUtils {
    public static String getName() {
        return "cas";
    }

    public static HashMap<String, String> getUserInformationCAS(String ticket){
        HashMap<String, String> userInformation = new HashMap<>();
        try {
            PropertiesReader propertiesReader = new PropertiesReader();
            String casUrl = propertiesReader.getPropOrDefault("cas.provider_url", "https://sso.ui.ac.id/cas2");
            String serviceUrl = propertiesReader.getPropOrDefault("cas.service_url", "https://amanah.cs.ui.ac.id");
        	TicketValidator validator = new Cas20ServiceTicketValidator(casUrl);
            Assertion assertion = validator.validate(ticket, serviceUrl);
            setUserInformationCAS(userInformation, assertion.getAttributes());
            setUserInformationCAS(userInformation, assertion.getPrincipal().getAttributes());
        } catch (Exception e) {
            System.out.println("Validation problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return userInformation;
    }

    private static void setUserInformationCAS(HashMap<String, String> userInformation, Map attrs) {
        PropertiesReader propertiesReader = new PropertiesReader();
        String nameKey = propertiesReader.getPropOrDefault("cas.name_key", "nama");
        if (attrs.get(nameKey) != null) userInformation.put("name", (String)attrs.get(nameKey));
        String emailKey = propertiesReader.getPropOrDefault("cas.email_key", "npm");
        if (attrs.get(emailKey) != null) userInformation.put("email", (String)attrs.get(emailKey));
        String idKey = propertiesReader.getPropOrDefault("cas.id_key", "npm");
        if (attrs.get(idKey) != null) userInformation.put("social_id", (String)attrs.get(idKey));
    }
}