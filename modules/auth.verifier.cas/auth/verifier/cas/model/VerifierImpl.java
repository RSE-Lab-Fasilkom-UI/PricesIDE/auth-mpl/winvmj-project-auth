package auth.verifier.cas;

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.jasig.cas.client.validation.TicketValidator;
import org.jasig.cas.client.validation.TicketValidationException;

import auth.verifier.core.VerifierComponent;
import auth.verifier.core.VerifierDecorator;
import auth.verifier.cas.adapters.CASPayloadAdapter;

import vmj.auth.core.AuthPayload;
import vmj.auth.core.TokenPayload;
import vmj.auth.utils.PropertiesReader;

public class VerifierImpl extends VerifierDecorator {
    
    public VerifierImpl(VerifierComponent verifier) {
		super(verifier);
        this.name = "cas";
	}
	
    @Override
    public AuthPayload verifyAndParse(final String token) {
        try {
            PropertiesReader propertiesReader = new PropertiesReader();
            String casUrl = propertiesReader.getPropOrDefault("cas.provider_url", "https://sso.ui.ac.id/cas2");
            String serviceUrl = propertiesReader.getPropOrDefault("cas.service_url", "https://amanah.cs.ui.ac.id");

        	TicketValidator validator = new Cas20ServiceTicketValidator(casUrl);
            Assertion assertion = validator.validate(token, serviceUrl);
            return new CASPayloadAdapter(assertion);
        } catch (TicketValidationException e) {
            System.out.println("Ticket validation problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return verifier.verifyAndParse(token);
    }

    @Override
    public boolean isValid(final String token) {
		try {
            DecodedJWT decodedJWT = JWT.decode(token);
            String verifierName = decodedJWT.getClaim("verifier").asString();
            if (verifierName != null) {
                return verifierName.equals(this.name);
            }
            return false;
        } catch (JWTDecodeException e) {
            if (token != null) {
                return token.startsWith("ST-");
            }
            return false;
        }
    }
}
