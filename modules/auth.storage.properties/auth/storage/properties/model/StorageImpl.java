package auth.storage.properties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import auth.storage.core.StorageComponent;
import auth.storage.core.StorageDecorator;

import vmj.auth.core.AuthPayload;
import vmj.auth.utils.PropertiesReader;

public class StorageImpl extends StorageDecorator {
	
	public StorageImpl(StorageComponent storage) {
        super(storage);
    }
	
	public StorageImpl() {
        super();
    }
	
    public Map<String, Object> getUserData(AuthPayload payload) {
        Map<String, Object> data = new HashMap<>();
        data.put("email", payload.getEmail());
        return data;
    }
    
    public Map<String, List<String>> getRoles(AuthPayload payload) {
        Map<String, List<String>> data = new HashMap<>();
        PropertiesReader propReader = new PropertiesReader();
        List<String> roles = propReader.getRolesFromEmail(payload.getEmail());

        for (String role : roles) {
            List<String> perms = propReader.getRolePerms(new String[]{role});
            data.put(role, perms);
        }
        return data;
    }
}
