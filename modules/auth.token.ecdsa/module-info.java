module auth.token.ecdsa {
	requires auth.token.core;
	exports auth.token.ecdsa;
	
	requires auth0.jwt;
	requires vmj.auth;
	requires vmj.routing.route;
}