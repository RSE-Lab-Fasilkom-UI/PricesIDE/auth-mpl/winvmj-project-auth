package auth.token.ecdsa;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.InvalidKeySpecException;

import com.auth0.jwt.algorithms.Algorithm;
import auth.token.core.TokenComponent;
import auth.token.core.TokenDecorator;
import auth.token.ecdsa.utils.KeyGeneratorUtils;

public class TokenImpl extends TokenDecorator {

	private final String name = "EC";
	private final int keyLength = 256;
	
	public TokenImpl(TokenComponent token) {
		super(token);
	}
	
	public TokenImpl() {
		super();
	}
	
	@Override
	public Algorithm getSigningAlgorithm() {
		ECPrivateKey privateKey = null;
		try {
			privateKey = getPrivateKey();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.out.println("Token problem: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
    	return Algorithm.ECDSA256(null, privateKey);
    }
	
	@Override
	public Algorithm getValidationAlgorithm() {
		ECPublicKey publicKey = null;
		try {
			publicKey = getPublicKey();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.out.println("Token problem: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
        return Algorithm.ECDSA256(publicKey, null);
    }
	
	private ECPrivateKey getPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
		PrivateKey privateKey = KeyGeneratorUtils.getPrivateKey(name, keyLength);
		return (ECPrivateKey) privateKey;
	}
	
	private ECPublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
		PublicKey publicKey = KeyGeneratorUtils.getPublicKey(name, keyLength);
        return (ECPublicKey) publicKey;
    }
}
