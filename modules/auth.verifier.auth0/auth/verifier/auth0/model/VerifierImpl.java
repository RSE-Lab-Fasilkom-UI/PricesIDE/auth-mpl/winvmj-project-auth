/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth.verifier.auth0;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.security.interfaces.RSAPublicKey;

import auth.verifier.core.VerifierComponent;
import auth.verifier.core.VerifierDecorator;
import auth.verifier.auth0.adapters.JWTPayloadAdapter;

import vmj.auth.core.AuthPayload;
import vmj.auth.core.TokenPayload;
import vmj.auth.utils.PropertiesReader;

/**
 *
 * @author ichla
 */
public class VerifierImpl extends VerifierDecorator {
    
    public VerifierImpl(VerifierComponent verifier) {
		super(verifier);
        this.name = "auth0";
	}

    @Override
    public AuthPayload verifyAndParse(final String token) {
        PropertiesReader propertiesReader = new PropertiesReader();
        String address = "https://" + propertiesReader.getProp("auth0.domain") + "/";
        String clientId = propertiesReader.getClientId(this.name);
        try {
            DecodedJWT jwt = JWT.decode(token.trim());
            JwkProvider provider = new UrlJwkProvider(address);
            Jwk jwk = provider.get(jwt.getKeyId());
            Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(address)
                    .build(); //Reusable verifier instance
            jwt = verifier.verify(token.trim());

            TokenPayload payload = null;
            if (jwt != null) {
                payload = new JWTPayloadAdapter(jwt);
                if (!payload.getAudiences().contains(clientId) || !payload.getIssuer().equals(address)) {
                    throw new IllegalArgumentException("Client ID mismatch");
                }
            } else {
                throw new IllegalArgumentException("Invalid ID token.");
            }
            return payload;
        } catch (JWTVerificationException e) {
            System.out.println("Token problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            System.out.println("Token problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (JwkException e) {
            System.out.println("Security issue: " + e.getLocalizedMessage());
            e.printStackTrace();
        }

        return verifier.verifyAndParse(token);
    }

    @Override
    public boolean isValid(final String token) {
		try {
            DecodedJWT decodedJWT = JWT.decode(token);
            String verifierName = decodedJWT.getClaim("verifier").asString();
            if (verifierName != null) {
                return verifierName.equals(this.name);
            }
            PropertiesReader propertiesReader = new PropertiesReader();
            String issuer = decodedJWT.getIssuer();
            if (issuer != null) {
                String address = propertiesReader.getProp("auth0.domain");
                return issuer.contains(address);
            }
            return false;
        } catch (JWTDecodeException e) {
            return false;
        }
    }
}
