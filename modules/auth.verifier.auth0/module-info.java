module auth.verifier.auth0 {
	requires auth.verifier.core;
	exports auth.verifier.auth0;
	
	requires auth0.jwt;
	requires auth0.jwk;
	requires vmj.auth;
}