package auth.verifier.ldap;

import java.util.Arrays;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import auth.verifier.core.VerifierComponent;
import auth.verifier.core.VerifierDecorator;

import vmj.auth.core.AuthPayload;

public class VerifierImpl extends VerifierDecorator {
    
    public VerifierImpl(VerifierComponent verifier) {
		super(verifier);
        this.name = "ldap";
	}
	
    @Override
    public AuthPayload verifyAndParse(final String token) {
        return verifier.verifyAndParse(token);
    }

    @Override
    public boolean isValid(final String token) {
		try {
            DecodedJWT decodedJWT = JWT.decode(token);
            String verifierName = decodedJWT.getClaim("verifier").asString();
            if (verifierName != null)
                return verifierName.equals(this.name);
            return false;
        } catch (JWTDecodeException e) {
            return false;
        }
    }
}
