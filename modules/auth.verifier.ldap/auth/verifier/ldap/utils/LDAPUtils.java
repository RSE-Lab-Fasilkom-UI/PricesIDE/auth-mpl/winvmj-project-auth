package auth.verifier.ldap.utils;

import java.util.*;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import vmj.auth.utils.PropertiesReader;

public class LDAPUtils {
    public static String getName() {
        return "ldap";
    }

    public static HashMap<String, String> getUserInformation(String username, String password) {
        HashMap<String, String> userInformation = new HashMap<>();
        try {
            PropertiesReader propertiesReader = new PropertiesReader();
            String factory = propertiesReader.getPropOrDefault("ldap.factory", "com.sun.jndi.ldap.LdapCtxFactory");
            String providerUrl = propertiesReader.getPropOrDefault("ldap.provider_url", "ldap://ldap.forumsys.com");
            String version = propertiesReader.getPropOrDefault("ldap.version", "3");
            String auth = propertiesReader.getPropOrDefault("ldap.auth", "simple");
            String baseDn = propertiesReader.getPropOrDefault("ldap.base_dn", "dc=example,dc=com");
            String filter = propertiesReader.getPropOrDefault("ldap.filter", "(|(mail=[])(uid=[]))").replaceAll("\\[.*?\\]", username);
            
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, factory);
            env.put(Context.PROVIDER_URL, providerUrl);
            env.put(Context.SECURITY_AUTHENTICATION, auth);
            env.put("java.naming.ldap.version", version);
            
            DirContext context = new InitialDirContext(env);
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration<SearchResult> results = context.search(baseDn, filter, searchControls);
            if (results.hasMore()) {
                SearchResult result = results.next();
                String userDn = result.getNameInNamespace();
                env.put(Context.SECURITY_PRINCIPAL, userDn);
                env.put(Context.SECURITY_CREDENTIALS, password);
                context = new InitialDirContext(env);
                context.close();
                setUserInformationLDAP(userInformation, result.getAttributes());
            }
        } catch (Exception e) {
            System.out.println("Validation problem: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return userInformation;
    }

    private static void setUserInformationLDAP(HashMap<String, String> userInformation, Attributes attrs) throws NamingException {
        PropertiesReader propertiesReader = new PropertiesReader();
        String nameKey = propertiesReader.getPropOrDefault("ldap.name_key", "cn");
        if (attrs.get(nameKey) != null) userInformation.put("name", (String)attrs.get(nameKey).get());
        String emailKey = propertiesReader.getPropOrDefault("ldap.email_key", "mail");
        if (attrs.get(emailKey) != null) userInformation.put("email", (String)attrs.get(emailKey).get());
        String idKey = propertiesReader.getPropOrDefault("ldap.id_key", "uid");
        if (attrs.get(idKey) != null) userInformation.put("social_id", (String)attrs.get(idKey).get());
    }
}