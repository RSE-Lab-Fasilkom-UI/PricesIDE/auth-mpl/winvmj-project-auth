module auth.verifier.ldap {
	requires auth.verifier.core;
	exports auth.verifier.ldap;
	exports auth.verifier.ldap.utils;

	requires jdk.naming.ldap;
	requires java.naming;
	requires java.persistence;
	requires vmj.auth;
}